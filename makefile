CC=g++
SOURCEDIR=src
BINDIR=bin
OBJDIR=obj
SOURCES=main.cpp distance.cpp search.cpp FSObject.cpp
LIBDIR=lib
FLAGS=--std=c++11 -Wall -O2 -Iinclude
LINKFLAGS=

all: engine

clean: $(patsubst %.cpp, $(OBJDIR)/%.o, $(SOURCES)) 
	rm $^

$(OBJDIR)/%.o: $(SOURCEDIR)/%.cpp
	$(CC) $(FLAGS) -o $@ -c $^

engine: $(patsubst %.cpp, $(OBJDIR)/%.o, $(SOURCES))
	$(CC) -o $(BINDIR)/$@ $^ $(FLAGS) $(LINKFLAGS)

