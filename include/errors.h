
void log(const char* msg, const char* trigger, const char* file, const int line);

#define tassert(a, msg) do {\
	if(!(a))\
		log((msg), #a, __FILE__, __LINE__);\
}while(0)
