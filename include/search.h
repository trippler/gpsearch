#ifndef search_h
#define search_h
#include <vector>
typedef int percent;
class FSObject;

typedef struct{
	percent match_percent;
	FSObject* object;
}entity_match;

bool operator<(const entity_match& a, const entity_match& b);

typedef struct{
	std::vector<entity_match> entities;
}matches;

void matches_sort(matches& m);

#endif

