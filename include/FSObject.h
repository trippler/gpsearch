#ifndef FSOBJECT_H
#define FSOBJECT_H
#include <string>
#include <vector>
#include "search.h"
enum FS_TYPE{
	FS_DIR,
	FS_FILE
};

class FSObject{
	public:
	FSObject(const char* name, FS_TYPE t, int level, FSObject* parent);
	~FSObject();
	FSObject* add_sub_object(const char* name, FS_TYPE t);
	std::string get_name();
	matches get_matches(const char* querry, int length);
	std::string get_full_path();
	private:
	std::string name;
	FS_TYPE type;
	std::vector<FSObject*> subs;
	int level;
	FSObject* parent;
};

#endif
