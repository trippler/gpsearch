#include <stdio.h>

void log(const char* msg, const char* trigger, const char* file, const int line){
	printf("[%s:%d]: ERROR: \"%s\" returned false. Message: %s\n", file, line, trigger, msg);
}

