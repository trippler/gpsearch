#include <string>
#include <stdio.h>
#include <string.h>
#include "FSObject.h"
#include "distance.h"
FSObject::FSObject(const char* name, FS_TYPE t, int level, FSObject* parent){
	this->name = name;
	this->type = t;
	this->level = level;
	this->parent = parent;
	subs = std::vector<FSObject*>();
	//printf("%*s %s%c\n", level*4, "", name, (t == FS_DIR)?'/':' ');
}

FSObject::~FSObject(){
	for(FSObject* f : subs)
		delete f;
}

FSObject* FSObject::add_sub_object(const char* name, FS_TYPE t){
	for(FSObject* f : subs){
		if(!strcmp(f->get_name().c_str(), name))
			return f;
	}
	subs.push_back(new FSObject(name, t, level+1, this));
	return subs.back();
}

std::string FSObject::get_name(){
	return name;
}

matches FSObject::get_matches(const char* querry, int length){
	matches m;
	m.entities.push_back({
	.match_percent = (length == 0 && this->name.size() == 0)? 100 : similarity_percent(querry, length, this->name.c_str(), this->name.size()),
	.object = this
	});
	for(FSObject* f : subs){
		matches n = f->get_matches(querry, length);
		m.entities.insert(m.entities.end(), n.entities.begin(), n.entities.end());
	}
	return m;
}

std::string FSObject::get_full_path(){
	if(parent)
		return parent->get_full_path() + "/" + get_name();
	return get_name();
}
