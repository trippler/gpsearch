#include <algorithm>
#include "search.h"

bool operator<(const entity_match& a, const entity_match& b){
	return a.match_percent > b.match_percent;
}


void matches_sort(matches& m){
	std::sort(m.entities.begin(), m.entities.end());
}
