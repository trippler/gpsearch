#include <string.h>
#include <ctype.h>
#define MAX(a,b) ((a)>(b)? a : b)
#define MIN(a,b) ((a)<(b)? a : b)

int distance (const char * word1, int len1, const char * word2, int len2){
	int matrix[len1 + 1][len2 + 1];
	int i;
	for (i = 0; i <= len1; i++) {
		matrix[i][0] = i;
	}
	for (i = 0; i <= len2; i++) {
		matrix[0][i] = i;
	}
	for (i = 1; i <= len1; i++) {
		int j;
		char c1;
		c1 = word1[i-1];
		for (j = 1; j <= len2; j++) {
			char c2;

			c2 = word2[j-1];
			if ((c1 | ' ') == (c2 | ' ')) {
				matrix[i][j] = matrix[i-1][j-1];
			}
			else {
				int del;
				int insert;
				int substitute;
				int minimum;

				del = matrix[i-1][j] + 1;
				insert = matrix[i][j-1] + 1;
				substitute = matrix[i-1][j-1] + 1;
				minimum = del;
				if (insert < minimum) {
					minimum = insert;
				}
				if (substitute < minimum) {
					minimum = substitute;
				}
				matrix[i][j] = minimum;
			}
		}
	}

	//If word in lowercase contains entire querry in lowercase - reward it
	char w1[len1+1], w2[len2+1];
	for(int n = 0; n < len1; ++n)
		w1[n] = tolower(word1[n]);
	w1[len1] = 0;
	for(int n = 0; n < len2; ++n)
		w2[n] = tolower(word2[n]);
	w2[len2] = 0;
	if(strstr(w2, w1)){
		return matrix[len1][len2] - (MAX(len1,len2)-MIN(len1,len2))/2;
	}
	return matrix[len1][len2];
}

int similarity_percent(const char * word1, int len1, const char * word2, int len2){
	return 100-(100*distance(word1, len1, word2, len2))/(MAX(len1,len2));
}

