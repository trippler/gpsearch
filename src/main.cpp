#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <dirent.h>
#include <string>
#include "distance.h"
#include "search.h"
#include "FSObject.h"
#define USE_NFTW

#ifdef USE_NFTW
//#define _XOPEN_SOURCE 1
#define _XOPEN_SOURCE_EXTENDED 1
#include <ftw.h>
#include <unistd.h>
#endif

void process_directory(FSObject &root, std::string path);

int main(int argc, char* argv[]){
	const char* word1 = ".";
	if(argc >= 2)
		word1 = argv[1];
	FSObject root("", FS_FILE, -1, NULL);
	process_directory(root, word1);
	while(true){
		char buf[1024];
		if(!fgets(buf, 1023, stdin))
			return 0;
		buf[strlen(buf)-1] = 0;
		word1 = buf;
		printf("Searching for %s...\n", word1);
		matches m = root.get_matches(word1, strlen(word1));
		matches_sort(m);
		int n = 0;
		printf("==========Matches=========\n");
		for(entity_match e : m.entities){
			printf("%d%%\t %s\n", e.match_percent, e.object->get_full_path().c_str());
			if(++n > 10)break;
		}
	}
	return 0;
}

std::vector<FSObject*> objects;
long unsigned total_objects;
int callback(const char* file, const struct stat* sb, int flag, struct FTW* s){
	if(s->level >= (int)objects.size())
		objects.push_back(NULL);
	objects[s->level] = objects[s->level-((s->level > 0)?1:0)]->add_sub_object(file+s->base, (flag==FTW_F)?FS_FILE : FS_DIR);
	total_objects++;
	return FTW_CONTINUE;

}

void process_directory(FSObject &object, std::string path){
	objects.clear();
	objects.push_back(&object);
	total_objects = 0;
	int fds = getdtablesize() - 10;
	int flags = FTW_PHYS | FTW_ACTIONRETVAL;
	nftw(path.c_str(), callback, fds, flags);
	objects.clear();
	printf("Added a total of %lu objects\n", total_objects);
}

